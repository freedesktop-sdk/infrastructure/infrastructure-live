locals {
  runner_config_yml = templatefile("${path.module}/templates/runner/config.yml.tpl", {
    base_config_authorized_keys      = var.base_config_authorized_keys
    base_config_sshd_port            = var.sshd_port
    runner_config_concurrency        = var.runner_config_concurrency
    runner_config_output_limit       = var.runner_config_output_limit
    runner_config_default_image      = var.runner_config_default_image
    runner_config_tag_list           = var.runner_config_tag_list
    runner_config_registration_token = var.runner_config_registration_token
    runner_config_run_untagged       = var.runner_config_run_untagged
  })
  runner_cloud_cfg = templatefile("${path.module}/templates/cloud.cfg.tpl", {
    content = base64encode(local.runner_config_yml)
  })
  runner_config_sh = templatefile("${path.module}/templates/config.sh.tpl", {
    name = "runner"
  })
}

data "cloudinit_config" "runner_config" {
  part {
    content_type = "text/cloud-config"
    content      = local.runner_cloud_cfg
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.runner_config_sh
  }
}

resource "openstack_compute_servergroup_v2" "runner-sg" {
  count    = var.runner_instance_count > 0 ? 1 : 0
  name     = "runner-sg-${var.environment}"
  policies = ["soft-anti-affinity"]
}

resource "openstack_compute_instance_v2" "runner" {
  count           = var.runner_instance_count
  name            = "osuosl-fdsdk-runner-${var.environment}-${count.index + 1}"
  image_id        = var.runner_image_id
  flavor_name     = var.runner_flavor
  key_pair        = var.key_pair
  security_groups = [openstack_networking_secgroup_v2.live_secgroup.name]
  user_data       = data.cloudinit_config.runner_config.rendered

  scheduler_hints {
    group = resource.openstack_compute_servergroup_v2.runner-sg[0].id
  }

  block_device {
    uuid                  = var.runner_image_id
    source_type           = "image"
    destination_type      = "local"
    boot_index            = 0
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_size           = 100
    guest_format          = "xfs"
    boot_index            = -1
    volume_type           = "ssd"
    delete_on_termination = true
  }

  metadata = {
    ssh_user  = var.ssh_user
    sshd_port = var.sshd_port
  }

  connection {
    user = try(self.metadata["ssh_user"], "debian")
    host = self.access_ip_v4
    port = try(self.metadata["sshd_port"], 22)
  }

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "sudo gitlab-runner unregister --all-runners"
    ]
    on_failure = continue
  }

  network {
    name = "general_servers2"
  }
}

output "runner_ip_addresses" {
  value = resource.openstack_compute_instance_v2.runner[*].access_ip_v4
}
