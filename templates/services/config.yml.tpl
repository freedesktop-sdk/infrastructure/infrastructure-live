---
base_config_authorized_keys:
%{ for key in base_config_authorized_keys ~}
  - ${key}
%{ endfor ~}
base_config_additional_mounts:
  - device: "{{ ansible_ec2_block_device_mapping_ebs0 }}"
    path: /srv
    type: xfs
base_config_sshd_port: ${base_config_sshd_port}
marge_config_api_token: ${marge_config_api_token}
marge_config_ssh_key: |
  ${replace(marge_config_ssh_key, "/\n/", "\n  ")}
marge_config_branches:
%{ for branch in marge_config_branches ~}
  - ${branch}
%{ endfor ~}
marge_config_container_tag: ${marge_config_container_tag}
marge_config_gitlab_url: ${marge_config_gitlab_url}
marge_config_ci_timeout: ${marge_config_ci_timeout}
lorry_config_api_token: ${lorry_config_api_token}
lorry_config_ssh_key: |
  ${replace(lorry_config_ssh_key, "/\n/", "\n  ")}
lorry_config_repo: ${lorry_config_repo}
lorry_config_downstream_domain: ${lorry_config_downstream_domain}
lorry_config_db_root: ${lorry_config_db_root}
lorry_config_working_root: ${lorry_config_working_root}
