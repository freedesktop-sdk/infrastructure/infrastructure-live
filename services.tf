locals {
  services_config_yml = templatefile("${path.module}/templates/services/config.yml.tpl", {
    base_config_authorized_keys    = var.base_config_authorized_keys
    base_config_sshd_port          = var.sshd_port
    marge_config_api_token         = var.marge_config_api_token
    marge_config_ssh_key           = var.marge_config_ssh_key
    marge_config_branches          = var.marge_config_branches
    marge_config_container_tag     = var.marge_config_container_tag
    marge_config_gitlab_url        = var.marge_config_gitlab_url
    marge_config_ci_timeout        = var.marge_config_ci_timeout
    lorry_config_api_token         = var.lorry_config_api_token
    lorry_config_ssh_key           = var.lorry_config_ssh_key
    lorry_config_repo              = var.lorry_config_repo
    lorry_config_downstream_domain = var.lorry_config_downstream_domain
    lorry_config_db_root           = var.lorry_config_db_root
    lorry_config_working_root      = var.lorry_config_working_root
  })
  services_cloud_cfg = templatefile("${path.module}/templates/cloud.cfg.tpl", {
    content = base64encode(local.services_config_yml)
  })
  services_config_sh = templatefile("${path.module}/templates/config.sh.tpl", {
    name = "services"
  })
}

data "cloudinit_config" "services_config" {
  part {
    content_type = "text/cloud-config"
    content      = local.services_cloud_cfg
  }

  part {
    content_type = "text/x-shellscript"
    content      = local.services_config_sh
  }
}

resource "openstack_blockstorage_volume_v3" "services_data" {
  count       = var.services_instance_count
  name        = "services-data-${var.environment}"
  size        = 100
  volume_type = "ssd"
}

resource "openstack_compute_instance_v2" "services" {
  count           = var.services_instance_count
  name            = "osuosl-fdsdk-services-${var.environment}"
  image_id        = var.services_image_id
  flavor_name     = var.services_flavor
  key_pair        = var.key_pair
  security_groups = [openstack_networking_secgroup_v2.live_secgroup.name]
  user_data       = data.cloudinit_config.services_config.rendered

  block_device {
    uuid                  = var.services_image_id
    source_type           = "image"
    destination_type      = "local"
    boot_index            = 0
    delete_on_termination = true
  }

  block_device {
    uuid             = openstack_blockstorage_volume_v3.services_data[count.index].id
    source_type      = "volume"
    destination_type = "volume"
    boot_index       = -1
  }

  network {
    name = "general_servers1"
  }
}

output "services_ip_address" {
  value = resource.openstack_compute_instance_v2.services[*].access_ip_v4
}
