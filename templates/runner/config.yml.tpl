---
base_config_authorized_keys:
%{ for key in base_config_authorized_keys ~}
  - ${key}
%{ endfor ~}
base_config_additional_mounts:
  - device: "{{ ansible_ec2_block_device_mapping_ebs0 }}"
    path: /srv
    type: xfs
base_config_podman_volume_path: "/srv/containers/volumes"
base_config_sshd_port: ${base_config_sshd_port}
runner_config_concurrency: ${runner_config_concurrency}
runner_config_output_limit: ${runner_config_output_limit}
runner_config_default_image: ${runner_config_default_image}
runner_config_casd_loglevel: info
runner_config_tag_list:
  - "{{ ansible_hostname }}"
%{ for tag in runner_config_tag_list ~}
  - ${tag}
%{ endfor ~}
runner_config_registration_token: ${runner_config_registration_token}
runner_config_run_untagged: ${runner_config_run_untagged}
