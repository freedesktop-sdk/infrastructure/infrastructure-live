# Common Variables

variable "environment" {
  type    = string
  default = "default"
}

variable "key_pair" {
  type    = string
  default = "jjardon_gnome"
}

variable "ssh_user" {
  type    = string
  default = "debian"
}

variable "sshd_port" {
  type    = number
  default = 20002
}

variable "base_config_authorized_keys" {
  type = list(string)
  default = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHo8uMY70SELbJUHNa77tADL1dhnpCJRG4JJU1Gjm7er fd-sdk-infrastructure",
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILLmU8L+CyK2PdLn8KW/Egw8C2QqSYmv7zXDC+amQA58 benbrown@deprave",
    "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAstAmEbkOucITFnUgeF1u0StLbNnEk38wADJ0rQaZ5DKGCYe5v9PgruawgmsPMnZBbqSIrtmRgAncyks3I+RdxKeasL97Fl3NvW6x4m+CORc5Ymz1omAtIVoYyZjtVe1BxNxU0D+J44N1C7sdSs/KGV2Jxcj5cj+vsoNjWtwO0FtQnsJRXB4nJgQYawdnaktSFR1vyml+Dyyv9IsD2TMoc5fDpWxsGK/Cfy9sEB6XOG+zcLB+UpFDAPh5h13H2jTWhtDZuKDXWxSws9GYuGA2X4j8AhQjnUGJLbl4bu7PFrYXz1dq2qgZuw4qIu0keH4euPK7g4Vbn36AcD92s25H0w== jjardon@gnome.org",
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZZPpbXfx7frSHUAFAM2vxgR5G1BQHprN6pm7M0268AJ7Mkatt8TXJ+AHY1S2XWGlcAIfshLrxzXGI0cALIHoFmtyYWQsfMRslPROqdnMeSGYqW/b1zjyyFoU9PHNG20ppOh+wR7rY4iOTXMKOsYgA5/BkpZ86jcSR9t43KCZlDYv8jKRN0FBYZUPkNsO/DlEsAV1cGsaShi1ZE4hOFEgaJPn5Rl9A8KKfRM5jaHOC+Ha97GqAgXaxbWJxhELt2Dxf/falWe51c/eClFvx+qGwiO/UzzKTQo+lQdgBW20h1ir1rxKk6Bd8p7K1uDrvWgjRqRQRYqCdtxKUaiDxI96p akitouni@gnome.org",
    "ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAEC1SNL2EltpHNcwGH0zVrLOeDZKXG2uFFul0a+iap3e0s4eB2K3xfsJTQF9IBpumW2nF9Odmxd42N3me1q0AsLtABBfA9qADEEOfJBF+t0Qs8IHymsTxgkFZnV3CZmEjeuG6Bc2zwy0muCvRgUVWeX1/0M2pjtvCrR7Td8qjQg5vQkxg== nanonyme@kiemura",
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM8Guuk9oPFx5MoSqe2i95S/+TZomYsYDHBrlxh8+nED dor.askayo@gmail.com"
  ]
}

# Runner Variables

variable "runner_image_id" {
  type = string
}

variable "runner_instance_count" {
  type    = number
  default = 5
}

variable "runner_flavor" {
  type    = string
  default = "r2.local.16c32m200d"
}

variable "runner_config_concurrency" {
  type    = number
  default = 1
}

variable "runner_config_output_limit" {
  type    = number
  default = 65536
}

variable "runner_config_default_image" {
  type    = string
  default = "alpine:latest"
}

variable "runner_config_tag_list" {
  type = list(string)
  default = [
    "packer",
    "riscv",
    "x86_64"
  ]
}

variable "runner_config_registration_token" {
  type = string
}

variable "runner_config_run_untagged" {
  type    = bool
  default = false
}

# Services Variables

variable "services_image_id" {
  type = string
}

variable "services_instance_count" {
  type    = number
  default = 1

  validation {
    condition     = var.services_instance_count == 0 || var.services_instance_count == 1
    error_message = "services_instance_count must be a value of 0 or 1."
  }
}

variable "services_flavor" {
  type    = string
  default = "m1.medium"
}

## Marge Variables

variable "marge_config_api_token" {
  type = string
}

variable "marge_config_ssh_key" {
  type = string
}

variable "marge_config_branches" {
  type = list(string)
  default = [
    "release/23.08",
    "release/24.08",
    "master"
  ]
}

variable "marge_config_container_tag" {
  type    = string
  default = "latest"
}

variable "marge_config_gitlab_url" {
  type    = string
  default = "https://gitlab.com"
}

variable "marge_config_ci_timeout" {
  type    = string
  default = "2880min"
}

## Lorry Variables

variable "lorry_config_api_token" {
  type = string
}

variable "lorry_config_ssh_key" {
  type = string
}

variable "lorry_config_repo" {
  type    = string
  default = "https://gitlab.com/freedesktop-sdk/infrastructure/mirroring-config.git"
}

variable "lorry_config_downstream_domain" {
  type    = string
  default = "gitlab.com"
}

variable "lorry_config_db_root" {
  type    = string
  default = "/srv/db"
}

variable "lorry_config_working_root" {
  type    = string
  default = "/srv/working-area"
}
