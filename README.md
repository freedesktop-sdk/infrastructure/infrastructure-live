# infrastructure-live

## Configuring SSH for Freedesktop SDK Hosts

Add the following to the top of your `~/.ssh/config`:

```
Include config.d/*
```

Then create the directory:

```
mkdir -p ~/.ssh/config.d
```

Run the script that will download the generated SSH config from the latest CI
artifacts:

```
./scripts/download-ssh-config > ~/.ssh/config.d/fd-sdk-live
```

You may additionally want to add a section to `~/.ssh/config.d/fd-sdk-local`
which disables host key checks (they are replaced whenever the instances are
recreated):

```
Host osuosl-fdsdk-*
    UserKnownHostsFile /dev/null
    StrictHostKeyChecking no
```
