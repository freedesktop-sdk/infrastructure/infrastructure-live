#!/bin/bash

set -e

/opt/ansible/.venv/bin/ansible-playbook /opt/ansible/${name}_config.yml
