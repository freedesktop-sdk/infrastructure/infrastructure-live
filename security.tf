resource "openstack_networking_secgroup_v2" "live_secgroup" {
  name                 = "fdsdk-secgroup-${var.environment}"
  description          = "Security group for infrastructure-live"
  delete_default_rules = true
}

resource "openstack_networking_secgroup_rule_v2" "egress_v4" {
  direction         = "egress"
  ethertype         = "IPv4"
  security_group_id = openstack_networking_secgroup_v2.live_secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "egress_v6" {
  direction         = "egress"
  ethertype         = "IPv6"
  security_group_id = openstack_networking_secgroup_v2.live_secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "ssh_v4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = var.sshd_port
  port_range_max    = var.sshd_port
  security_group_id = openstack_networking_secgroup_v2.live_secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "ssh_v6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = var.sshd_port
  port_range_max    = var.sshd_port
  security_group_id = openstack_networking_secgroup_v2.live_secgroup.id
}
