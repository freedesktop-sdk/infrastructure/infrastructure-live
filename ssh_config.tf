resource "local_file" "ssh_config" {
  filename = "${path.root}/ssh_config"
  content = templatefile("${path.root}/templates/ssh_config.tftpl", {
    hosts = concat(
      resource.openstack_compute_instance_v2.runner[*].name,
      resource.openstack_compute_instance_v2.services[*].name
    )
    ips = concat(
      resource.openstack_compute_instance_v2.runner[*].access_ip_v4,
      resource.openstack_compute_instance_v2.services[*].access_ip_v4
    )
    ssh_user  = var.ssh_user
    sshd_port = var.sshd_port
  })
}
